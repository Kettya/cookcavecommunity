@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="card radius">
                    <h3 class="card-header"> {{Auth::user()->name}}</h3>
                    @foreach($myRecipes as $recipe)
                        <div class="card-block table-responsive">
                            <table class="table">

                                <tr>
                                    <td><a class="nav-link"
                                           href="{{ route('selected-recipe', [$recipe->id]) }}">{{ $recipe->name }}</a></td>
                                </tr>

                                <tr>
                                </tr>
                            </table>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-sm-6">
                <form action="{{ route('save-avatar') }}" method="post">
                    @csrf
                    <div class="table-responsive">
                        <table class="table table-stiped">
                            @foreach($avatars as $avatar)
                                @if($avatar->image !== null)
                                    <tr>
                                        <label class="avatar-selector">
                                            <input type="radio" name="avatar-selector" value="{{ $avatar->id }}"
                                                   @if(Auth::user()->avatar_id == $avatar->id) checked @endif>
                                            <img src="{{ url($avatar->image) }}" alt="">
                                        </label>
                                    </tr>
                                @else

                                @endif
                            @endforeach

                                    <div class="form-group">
                                        <input type="submit" value="Save" class="btn btn-primary mb-2">
                                    </div>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection