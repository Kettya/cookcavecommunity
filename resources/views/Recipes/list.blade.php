@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card radius">
                    <h3 class=" card-header">Uploaded recipes</h3>
                    <div class="card-block table-responsive">
                        <table class="table">
                            <tbody>
                            @foreach($recipes as $recipe)
                                <tr>
                                    <td><a class="nav-link" href="{{ route('selected-recipe', [$recipe->id]) }}">{{ $recipe->name }}</a> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection