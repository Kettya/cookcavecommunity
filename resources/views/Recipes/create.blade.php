@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post"enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="fName">* Food Name:</label>
                        <input type="text" class="form-control" id="fName" name="fName" required/>
                    </div>
                    <div class="form-group">
                        <label for="ingredients">* Ingredients</label>
                        <textarea class="form-control" id="ingredients" name="ingredients" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="preparation">* Preparation</label>
                        <textarea class="form-control" id="preparation" name="preparation" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="pictures">Picture from the food</label>
                        <input type="file" class="form-control-file" id="pictures" name="pictures">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Upload" class="btn btn-primary mb-2"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

