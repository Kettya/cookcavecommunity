@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card radius" id="recipeTable">
                    <h3 class=" card-header">Recipe</h3>
                    <div class="card-block table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th colspan="3">{{ $recipe->name }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Ingredients</td>
                                <td> @include('recipes.ingredients',['ingredients'=>$recipe->ingredients]) </td>
                                @if($recipe->image !== null)
                                    <td rowspan="2">
                                        <img src="{{ $recipe->getThumbUrl() }}" alt="">
                                    </td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                            <tr>
                                <td>Preparation</td>
                                <td class="preparation">{{ $recipe->preparation }}</td>
                                @if($recipe->image === null)
                                    <td></td>
                                @endif
                            </tr>
                            <tr>
                                <td>Uploader</td>
                                <td>{{ $recipe->uploader->name }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div>
                                        <form action="{{ route('rate-recipe', [$recipe->id]) }}" method="post">
                                            @csrf
                                            <label class="rate">
                                                <input type="radio" name="rate" value="1">
                                                <img src="{{ url('Avatars/star.png') }}" alt="">
                                            </label>
                                            <label class="rate"><input type="radio" name="rate" value="2">
                                                <img src="{{ url('Avatars/star.png') }}" alt="">
                                            </label>
                                            <label class="rate">
                                                <input type="radio" name="rate" value="3">
                                                <img src="{{ url('Avatars/star.png') }}" alt="">
                                            </label>
                                            <label class="rate">
                                                <input type="radio" name="rate" value="4">
                                                <img src="{{ url('Avatars/star.png') }}" alt="">
                                            </label>
                                            <label class="rate">
                                                <input type="radio" name="rate" value="5">
                                                <img src="{{ url('Avatars/star.png') }}" alt="">
                                            </label>
                                            <div class="form-group">
                                                <input type="submit" value="Rate" class="btn btn-primary mb-2"/>
                                            </div>
                                        </form>
                                    </div>
                                </td>
                                <td>
                                    Rate: {{ round($rating_avg, 1) }} ({{ $rating_count }})
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="card"  id="commentTable">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <td>
                                    <form action="{{ route('comment-recipe', [$recipe->id]) }}" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label for="comment">Comment</label>
                                            <textarea class="form-control" id="comment" name="comment"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Comment" class="btn btn-primary mb-2"/>
                                        </div>
                                    </form>

                                </td>
                            </tr>
                            @foreach($comments as $comment)
                                <tr >
                                    <th>{{ $comment->user->name }}</th>
                                    <th>{{ $comment->created_at }}</th>
                                </tr>
                                <tr>
                                    <td colspan="2">{{ $comment->comment }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection