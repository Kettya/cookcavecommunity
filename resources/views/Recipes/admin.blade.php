@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="card">
                <h3 class=" card-header">Approve</h3>
                @foreach($unapproved as $approve)
                   <div class="card">
                       <div class="col-md-12">
                           <div class="card-block table-responsive">
                               <table class="table">
                                   <thead>
                                   <tr>
                                       <th colspan="3">{{ $approve->name }}</th>
                                   </tr>
                                   </thead>
                                   <tbody>
                                   <tr>
                                       <td>Ingredients</td>
                                       <td> @include('recipes.ingredients',['ingredients'=>$approve->ingredients]) </td>
                                       @if($approve->image !== null)
                                           <td rowspan="2">
                                               <img src="{{ $approve->getThumbUrl() }}" alt="">
                                           </td>
                                       @else
                                           <td></td>
                                       @endif
                                   </tr>
                                   <tr>
                                       <td>Preparation</td>
                                       <td class="preparation">{{ $approve->preparation }}</td>
                                       @if($approve->image === null)
                                           <td></td>
                                       @endif
                                   </tr>
                                   <tr>
                                       <td>Uploader</td>
                                       <td>{{ $approve->uploader->name }}</td>
                                       <td></td>
                                   </tr>
                                   <tr>
                                       <td colspan="2">
                                           <form action="{{ route('modify-recipe', [$approve->id]) }}" method="post">
                                               @csrf
                                               <div class="form-group">
                                                   <label for="">What do you want?</label>
                                                   <input type="submit" name="approve" value="Approve"
                                                          class="btn btn-primary mb-2">
                                                   <input type="submit" name="delete" value="Delete"
                                                          class="btn btn-primary mb-2">
                                               </div>
                                           </form>
                                       </td>
                                   </tr>
                                   </tbody>
                               </table>
                           </div>
                       </div>
                   </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection