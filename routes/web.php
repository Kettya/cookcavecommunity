<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@base');

Route::get('/recipes', 'RecipesController@getIndex')->name('recipes');


Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/recipe/create', 'RecipesController@getCreate')->name('create-recipes');
    Route::post('/recipe/create', 'RecipesController@postCreate');
    Route::get('/recipe/random', 'RecipesController@getRandom')->name('recipes-random');
    Route::get('/recipe/{id}','RecipesController@getRecipe')->name('selected-recipe');
    Route::post('/recipe/{id}/comment','RecipesController@commentCreate')->name('comment-recipe');
    Route::post('/recipe/{id}/rate','RecipesController@rate')->name('rate-recipe');
    Route::get('/my-profile', 'UserController@getUserData')->name('my-profile');
    //Route::get('/my-profile','UserController@getAvatar')->name('user-avatar');
    Route::post('/my-profile/save-avatar','UserController@setAvatar')->name('save-avatar');
    Route::get('/recipes/admin','RecipesController@approveRecipe')->name('approve-recipe');
    Route::post('/recipes/admin/modify-recipe/{id}','RecipesController@approve')->name('modify-recipe');
});