<?php


namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Recipe extends Model
{

    const THUMBNAIL_EXTENSION = '.thumb.jpg';

    protected $fillable = [
        'uploader_id',
        'name',
        'ingredients',
        'preparation',
        'image',
        'approved',
    ];

    public function uploader()
    {
        return $this->belongsTo(User::class, 'uploader_id');
    }

    public function getThumbUrl()
    {
        return url($this->image . self::THUMBNAIL_EXTENSION);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'recipes_id');
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'recipes_id');
    }

}

