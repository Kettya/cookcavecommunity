<?php


namespace App;

use Illuminate\Database\Eloquent\Model;
class Rating extends Model
{
    protected $fillable = [
        'rate',
        'user_id',
        'recipes_id',
    ];
}