<?php
/**
 * Created by PhpStorm.
 * User: ketty
 * Date: 4/7/2018
 * Time: 01:20
 */

namespace App\Http\Controllers;


use App\Avatar;
use App\Recipe;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends \Illuminate\Routing\Controller
{

    function getUserData()
    {
        $myRecipes = Auth::user()->uploadedRecipes;
        $avatars = Avatar::all();
        //return Auth::user()->uploadedRecipes;
        //return view('user.profile',Auth::user()->uploadedRecipes);
        //return Recipe::query()->where('uploader_id', Auth::user()->id)->get();
        /*return Recipe::query()->whereHas('uploader', function(Builder $builder) {
            return $builder->where('id', Auth::user()->id);
        })->get();*/
        return view('user.profile', [
            'myRecipes' => $myRecipes,
            'avatars' => $avatars,
        ]);

    }

    function setAvatar(Request $request)
    {
        Auth::user()->update([
            'avatar_id' => $request->post('avatar-selector'),
        ]);
        return redirect()->back();

    }

}