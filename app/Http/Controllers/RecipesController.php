<?php
/**
 * Created by PhpStorm.
 * User: ketty
 * Date: 3/6/2018
 * Time: 23:23
 */

namespace App\Http\Controllers;


use App\Comment;
use App\Rating;
use App\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class RecipesController extends \Illuminate\Routing\Controller
{
    public function getIndex()
    {
        $recipes = Recipe::all()
            ->where('approved', 1);
        return view('recipes.list', [
            'title' => 'Recipe list',
            'recipes' => $recipes,
        ]);
    }

    public function getCreate()
    {
        return view('recipes.create');
    }


    public function postCreate(Request $request)
    {
        Validator::make($request->all(), [
            'fName' => ['required'],
            'ingredients' => ['required'],
            'preparation' => ['required']
        ]);
        // = $request->file('photo');
        // $file = $request->photo;
        $path = null;
        // $thumbPath = null;
        $smallImage = null;
        if ($request->hasFile('pictures') && $request->file('pictures')->isValid()) {
            $picture = $request->file('pictures');
            $path = Storage::disk('local')->put('RecipePhotos', $picture);
            $smallImage = Image::make($picture);
            $smallImage->fit(400, 300);
            Storage::disk('local')->put($path . Recipe::THUMBNAIL_EXTENSION, $smallImage->encode('jpg'));
        }
        if ($request->post('fName') !== null && $request->post('ingredients') !== null && $request->post('preparation') !== null) {
            \App\Recipe::create([
                'uploader_id' => $request->user()->id,
                'name' => $request->post('fName'),
                'ingredients' => $request->post('ingredients'),
                'preparation' => $request->post('preparation'),
                'image' => $path,
            ]);

            session()->flash('message', 'Succesfully uploaded');
            return redirect(route('create-recipes'));
        }
    }

    public function getRandom()
    {
        $randomRecipe = Recipe::query()->inRandomOrder()->first();
        return $this->getRecipeView($randomRecipe);
    }

    public function getRecipe($id)
    {
        $recipe = Recipe::query()->findOrFail($id);
        return $this->getRecipeView($recipe);
    }

    private function getRecipeView(Recipe $recipe) {
        $comments = $recipe->comments;
        $ratingAvg = $recipe->ratings()->avg('rate');
        $ratingCount = $recipe->ratings()->count();
        return view('recipes.recipe', [
            'title' => 'Recipe ',
            'recipe' => $recipe,
            'comments' => $comments,
            'rating_avg' => $ratingAvg,
            'rating_count' => $ratingCount,
        ]);
    }

    public function approveRecipe()
    {
        $unapproved = Recipe::all()
            ->where('approved', 0);
        return view('recipes.admin', ['unapproved' => $unapproved]);
    }

    public function approve(Request $request, $id)
    {
        if ($request->post('approve')) {
            Recipe::query()->findOrFail($id)->update(['approved' => true]);
        } elseif ($request->post('delete')) {
            Recipe::query()->findOrFail($id)->delete();
        }
        return redirect()->back();
    }

    public function commentCreate(Request $request, $id)
    {
        Validator::make($request->all(), ['comment' => ['required']]);

        if ($request->post('comment') !== null) {
            \App\Comment::create([
                'user_id' => $request->user()->id,
                'recipes_id' => $id,
                'comment' => $request->post('comment')
            ]);
        }
        return redirect()->back();

    }

    public function rate(Request $request, $id)
    {
        Validator::make($request->all(), ['rate' => ['required']]);
        if ($request->post('rate') !== null) {
            \App\Rating::create([
                'rate' => $request->post('rate'),
                'user_id' => $request->user()->id,
                'recipes_id' => $id,
            ]);
        }
        return redirect()->back();
    }
}