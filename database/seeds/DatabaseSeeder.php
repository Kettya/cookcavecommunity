<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Foo',
            'email' => 'foo@bar.baz',
            'password' => Hash::make('q1w2e3r4'),
        ]);
        // $this->call(UsersTableSeeder::class);
        $user = \App\User::create([
            'name' => 'Kety',
            'email' => 'kicsitenshi@gmail.com',
            'password' => Hash::make('abc123'),
        ]);
        \App\Recipe::create([
            'uploader_id' => $user->id,
            'name' => 'szecsu',
            'ingredients' => "Csirke\nRizs",
            'preparation' => 'Főzd meg és finom legyen, meg ugyebár csípjen, mint a fene.',
        ]);

        \App\Recipe::create([
            'uploader_id' => $user->id,
            'name' => 'Tea',
            'ingredients' => "Víz\nTea",
            'preparation' => 'Forrald fel a vizet és tégy bele teát. Kész!',
        ]);
    }
}
